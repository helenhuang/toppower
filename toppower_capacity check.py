from turtle import title
import pandas as pd
import glob, os, openpyxl
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from plotly.io import write_html
import shutil
import plotly.express as px
import numpy as np
from scipy import integrate

def capacity_check(df):
    capacity_col = []
    capacity =0
    for i in range(1,df.shape[0]):
        y = df["Current(mA)"].tolist()
        x = df["Time(min)"]/60
        x = x.tolist()
        capacity += (y[i-1]+y[i])*(x[i]-x[i-1])/2
        capacity_col.append(capacity)
        if len(capacity_col)==df.shape[0]-1:
            capacity_col1 = [0]+capacity_col
    return capacity_col1 # convert to mAh
   

def toppower_vis():
    # Get parent folder name
    path = os.getcwd()
    # if "(" in os.path.basename(path): # identifier for the file name
    #     graph_title = os.path.basename(path)
    # else:
   
    graph_title =os.path.basename(os.path.dirname(os.path.dirname(path)))+ "_" + os.path.basename(os.path.dirname(path))+ "_" + os.path.basename(path)

    # Setting up plots 
    colors = px.colors.qualitative.Dark24
    colors1 = px.colors.qualitative.Light24
    
    fig = make_subplots(rows = 5, cols= 1, shared_xaxes= True, x_title="Time(s)", 
                        #vertical_spacing=0.25, #row_heights= [800,800,800],
                        )
    fig.update_layout(title_text = graph_title)
    fig.update_yaxes(title_text = "Voltage(V)",
                    col=1, row = 1)
    fig.update_yaxes(title_text = "Current(A)",
                        col=1, row = 2)
    fig.update_yaxes(title_text = "Capacity(Ah)",
                        col=1, row = 3)
    fig.update_yaxes(title_text = "Temperature(°C)",
                        col=1, row = 4)
    fig.update_yaxes(title_text = "Pressure(kPa)",
                        col=1, row = 5)
    fig2 = make_subplots(rows = 3, cols= 1, shared_xaxes= True, x_title="Time(s)", 
                        #vertical_spacing=0.25, #row_heights= [800,800,800],
                        )
    fig2.update_layout(xaxis_title = "Time(s)",
                    title = graph_title + "- Capacity check"
                    )
    fig2.update_yaxes(title_text = "Capacity(mAh)",
                    col=1, row = 1)
    fig2.update_yaxes(title_text = "Capacity difference(mAh)",
                        col=1, row = 2)
    fig2.update_yaxes(title_text = "Current(A)",
                        col=1, row = 3)

    # Get all .xlsx files in the folder and add them to the graphs
    file_list = sorted(glob.glob("*.xlsx"))
    temp_list =[]
    #file_list1 = [int(i.strip("CH").strip(".xlsx")) for i in file_list]
    channel_list = ["CH"+str(i) for i in range(1,25,1)]
    for i in range(len(channel_list)):
        
        df = pd.read_excel(channel_list[i]+".xlsx")
        # df2 = pd.read_excel(channel_list[i]+"_capacity check.xlsx")
        # df1 = pd.DataFrame({"dt":[],})
        df["Time(s)"] = df["Time(min)"]*60 

        # Get continuous capacity
        df["Corrected Capacity(mAh)"] = df["Capacity(mAh)"]
        steps = df["Step"].unique()
        df.update(df.loc[df["Current(mA)"]<0]["Corrected Capacity(mAh)"].apply(lambda x: x*(-1)))

      
        # Screening
        # time =  df["Time(s)"].tolist()
        # df["dt"] =[(time[i+1]-time[i]) for i in range(len(time)-1)] + [0]
        # new_df1 = df.loc[(df["dt"]> 0.7)]

        
        
        # # Heatmap z
        # t_max = int(df["Time(s)"].max())
        # temp =[]
        # for j in np.arange (0, t_max+1, 0.1):
        #     idx = df["Time(s)"].sub(j).abs().idxmin()
        #     temp.append(float(df['Temp(℃)'].iloc[idx]))
        # temp_list.append(temp)

        ext = ".xlsx"
        time_col_name = "Time(min)"
        trace_name = channel_list[i]
        #channel_list.append(trace_name)
        print(graph_title, channel_list[i])
        line_color = colors[i]

        # Capacity check
        for j in range(1,len(steps)):
            minidx= df.loc[df["Step"]==steps[j]].index.min()
            maxidx = df.loc[df["Step"]==steps[j]].index.max()
            last_capacity = df.loc[df["Step"]==steps[j-1]]["Corrected Capacity(mAh)"].iloc[-1]
            df.loc[minidx:maxidx,"Corrected Capacity(mAh)"] += last_capacity
        y = df["Current(mA)"].to_numpy()
        x = df["Time(min)"]/60
        x = x.to_numpy()
        df["TP Integrated capacity"] = capacity_check(df)
        df["python calculated capacity"] =[0]+ integrate.cumulative_trapezoid(x,y).tolist()
        print(len(df["python calculated capacity"]),len(df["TP Integrated capacity"] ))
        
        
        df["capacity method difference"] =  df["python calculated capacity"]- df["TP Integrated capacity"]
        df["capacity difference"] = df["Corrected Capacity(mAh)"] -df["python calculated capacity"]
        #df1 = df[["Corrected Capacity(mAh)","TP Integrated capacity","python calculated capacity","capacity difference","capacity method difference"]]
        df.to_excel(trace_name+"_capacity.xlsx")
        fig2.add_trace(go.Scatter(x = df[time_col_name]*60,
                                    y = df["Corrected Capacity(mAh)"],
                                    mode="lines",
                                    name=trace_name + " - TP capacity",
                                    line = dict(color=line_color), 
                                    legendrank=i+1,
                                    legendgroup=trace_name,  
                                legendgrouptitle_text=trace_name),
                                 row = 1, col =1,
                                    )
        fig2.add_trace(go.Scatter(x = df[time_col_name]*60,
                                    y = df["python calculated capacity"],
                                    mode="lines",
                                    name=trace_name + " - Integrated capacity",
                                    line = dict(color=colors1[i]), 
                                    legendrank=i+1,
                                    legendgroup=trace_name,  
                                legendgrouptitle_text=trace_name),
                                 row = 1, col =1,
                                    )
        fig2.add_trace(go.Scatter(x = df[time_col_name]*60,
                                    y = df["capacity difference"],
                                    mode="lines",
                                    name=trace_name + " - (TP - integrated)",
                                    line = dict(color=line_color), 
                                    legendrank=i+1,
                                    legendgroup=trace_name,  
                                legendgrouptitle_text=trace_name),
                                 row = 2, col =1,
                                    )
        fig2.add_trace(go.Scatter(x = df[time_col_name]*60,
                                    y = df["Current(mA)"]/1000,
                                    mode="lines",
                                    name=trace_name + " - Current",
                                    line = dict(color=line_color), 
                                    legendrank=i+1,
                                    legendgroup=trace_name,  
                                legendgrouptitle_text=trace_name),
                                    row = 3, col =1,)

        fig.add_trace(go.Scatter(x = df[time_col_name]*60,
                                    y = df["Votage(mV)"]/1000,
                                    mode="lines",
                                    name=trace_name + " - Voltage",
                                    line = dict(color=line_color), legendgroup=trace_name, 
                                    legendrank=i+1, 
                                legendgrouptitle_text=trace_name),
                                    row = 1, col =1,)

        fig.add_trace(go.Scatter(x = df[time_col_name]*60,
                                    y = df["Current(mA)"]/1000,
                                    mode="lines",
                                    name=trace_name + " - Current",
                                    line = dict(color=line_color), 
                                    legendrank=i+1,
                                    legendgroup=trace_name,  
                                legendgrouptitle_text=trace_name),
                                    row = 2, col =1,)
        fig.add_trace(go.Scatter(x = df[time_col_name]*60,
                                    y = df["Capacity(mAh)"]/1000,
                                    mode="lines",
                                    name=trace_name + " - Capacity",
                                    line = dict(color=line_color), 
                                    legendrank=i+1,
                                    legendgroup=trace_name,  
                                legendgrouptitle_text=trace_name),
                                    row = 3, col =1,)
        fig.add_trace(go.Scatter(x = df[time_col_name]*60,
                                y = df["Temp(℃)"],
                                mode="lines",
                                name=trace_name + " - Temperature",
                                line = dict(color=line_color),
                                legendrank=i+1,
                                legendgroup=trace_name,  
                                legendgrouptitle_text=trace_name),
                                row = 4, col =1,)
        fig.add_trace(go.Scatter(x = df[time_col_name]*60,
                                y = df["Pressure(kPa)"],
                                mode="lines",
                                name=trace_name + " - Pressure",
                                line = dict(color=line_color),
                                legendgroup=trace_name,  
                                legendrank=i+1,
                                legendgrouptitle_text=trace_name),
                                row = 5, col =1,)


    # fig_temp = go.Figure(data=go.Heatmap(z=np.array(temp_list),
    #             x=[i for i in np.arange(0, t_max+1, 0.1)],
    #             y=channel_list,
    #             hoverongaps = False))
    # fig_temp.update_layout(xaxis_title = "Time(s)", title_text = graph_title+" - heatmap")

    fig.update_layout(hovermode="x")
    fig2.update_layout(hovermode="x")
    html_name = graph_title + ".html"
    with open(html_name, 'w') as f:
        f.write(fig.to_html(full_html=False, include_plotlyjs='cdn'))
    with open(graph_title+"_capacity check.html", 'w') as f:
        f.write(fig2.to_html(full_html=False, include_plotlyjs='cdn'))
    # with open(graph_title+"_Heatmap.html", 'w') as f:
    #     f.write(fig_temp.to_html(full_html=False, include_plotlyjs='cdn'))
        
    #shutil.move(html_name, os.path.dirname(os.path.dirname(path))+"/HTML graphs/"+html_name)
    return graph_title

def screening(file,graph_title,  t_interval = 0.7): 
    df = pd.read_excel(file, usecols= ['Time(min)', 'Votage(mV)','Current(mA)', 'Capacity(mAh)','Step', "Pressure(kPa)", "Temp(℃)"])
    df["Time(s)"] = df["Time(min)"]*60 
    df1 = pd.DataFrame({"dt":[],})
    time =  df["Time(s)"].tolist()
    voltage =df['Votage(mV)'].tolist()
    current = df['Current(mA)'].tolist()
    df1["dt"] =[(time[i+1]-time[i]) for i in range(len(time)-1)] + [0]
    df1["dV"] =[(voltage[i+1]-voltage[i]) for i in range(len(time)-1)] + [0]
    df1["dI"] =[(current[i+1]-current[i]) for i in range(len(time)-1)] + [0]
    df1["Time(s)"] = df["Time(s)"]
    new_df = pd.merge(df, df1,how="outer", on="Time(s)")
    df2 = df1.loc[df1["dt"]>= t_interval]
    df5 = new_df.loc[(new_df["dt"]== 0)&(new_df["dI"]>0.1)&(new_df["Step"] != "001 REST")]
    df6 = new_df.loc[(new_df["dt"]== 0)&(new_df["dV"]>0.1)&(new_df["Step"] != "001 REST")]
    df3 = new_df.loc[(new_df["dt"]== 0)&(new_df["dI"]!=0)&(new_df["Step"] != "001 REST")]
    df4 = new_df.loc[(new_df["dt"]== 0)&(new_df["dV"]!=0)&(new_df["Step"] != "001 REST")]
    temp_range = abs(df["Temp(℃)"].max()-df["Temp(℃)"].min())
    p_range = abs(df["Pressure(kPa)"].max()-df["Pressure(kPa)"].min())
    I_range = df.loc[df["Current(mA)"] != 0]["Current(mA)"].max() - df.loc[df["Current(mA)"] != 0]["Current(mA)"].min()
    I_var_percentage = abs(I_range*100/140000)
    entry = [graph_title, file.strip(".xlsx"), len(df), len(df2), df1["dt"].max(), len(df4), len(df6), len(df3), len(df5), I_range ,I_var_percentage, 
    df["Temp(℃)"].mean(), temp_range, df["Pressure(kPa)"].mean(), p_range]

    return entry
#parent dir - where this script is 
pwd = os.getcwd()

# where all the graphs will go
output_path = pwd+"/HTML graphs/"
# df_screening = pd.DataFrame({"Test":[], "Channel ID":[], "total datapoints":[], ">0.7s spacing":[], "max. dt spacing(s)":[],"same t but different V":[],
# "same t but dV > 0.1mV":[], "same t but different I":[], "same t but dI > 0.1mA ":[], "max. I varaiation(mA)": [], "I variation%":[],
# "Average temperature(°C)": [], "max. temperature variation(°C)": [],  "Average pressure(kPa)": [], "max. pressure variation(kPa)": []})
if os.path.exists(output_path) == False:
    os.mkdir(output_path)
folder_list = glob.glob(pwd+"/*/")
#print(folder_list)
a = len(folder_list)
for i in range(a):
    os.chdir(folder_list[i])
    folder = folder_list[i]
    # data in the next subfolder
    if glob.glob(folder+"/*/"):
        print(folder)
        for folder in glob.glob(folder+"/*/"):
            print(folder)
            os.chdir(folder) # where the .xlsx files are
            wd = os.getcwd()
            for folder in glob.glob(folder+"/*/"):
                print(folder)
                os.chdir(folder) # where the .xlsx files are
                wd = os.getcwd()
                if glob.glob("*xlsx"):
                    # only process channel data
                    if any("CH" in file for file in glob.glob("*xlsx")):
                        path = os.getcwd()
                        test =os.path.basename(os.path.dirname(os.path.dirname(path)))+ "_" + os.path.basename(os.path.dirname(path))+ "_" + os.path.basename(path)
                        #test =  os.path.basename(os.path.dirname(path))+ "_" + os.path.basename(path)
                        #capacity_check()
                        toppower_vis()
                        # for file in glob.glob("*.xlsx"):
                        #     #entry = screening(file, test)
                        #     #df_screening.loc[len(df_screening)] = entry
                        #     #print(entry)
                        #     # with pd.ExcelWriter(pwd+"Data Screening_rbyr.xlsx", engine='openpyxl', mode='w') as writer: 
                        #     #     df_screening.to_excel(writer, index = False)
                        for file in glob.glob("*.html"):
                            shutil.copy2(file, output_path)
                            #os.remove(file)
                        os.chdir(pwd)
    # data in this subfolder
    # else: 
    #     print(folder)
    #     os.chdir(folder) # where the .xlsx files are
    #     if glob.glob("*xlsx"):
    #         # only process exported data
    #         if any("CH" in file for file in glob.glob("*xlsx")):
    #             #test = os.path.basename(path)
    #             test = toppower_vis()
    #             for file in glob.glob("*xlsx"):
    #                 entry = screening(file, test)
    #                 df_screening.loc[len(df_screening)] = entry
    #                 print(entry)
    #                 # with pd.ExcelWriter(pwd+"Data Screening_rbyr.xlsx", engine='openpyxl', mode='w') as writer: 
    #                 #     df_screening.to_excel(writer, index = False)
    #             for file in glob.glob("*.html"):
    #                     shutil.copy2(file, output_path)
    #                     #os.remove(file)
    #             os.chdir(pwd)
    
# os.chdir(pwd)
# df_screening.to_excel("Data Screening.xlsx", index = False)
